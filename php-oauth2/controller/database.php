<?php

namespace database_connection;

require 'vendor/autoload.php';

use \methods_encryption\methodUtility as methodUtility;

class databaseUtility{

    private $hostname;
    private $username;
    private $password;
    private $connection;

    public function __construct($hostname, $username, $password) {
        $this->hostname = $hostname;
        $this->username = $username;
        $this->password = $password;
        $this->connection = new \mysqli($this->hostname, $this->username, $this->password);
        if ($this->connection->connect_error) {
            die("CONNECTION FAILED: " . $this->connection->connect_error);
        }
    }

    // PARSE GET REQUEST
    function parseURL($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec($ch), true);
        curl_close($ch);
        return $data;
    }

    function insertClient($client_pkce_code) {
        $client_id = hash('sha512', uniqid());
        $client_auth_code = hash('sha256', uniqid());
        $query = "INSERT INTO `meesho`.`client` (`client_serial_no`, `client_id`, `user_id`, `client_access_token`, `client_refresh_token`, `client_auth_code`, `client_pkce_code`) VALUES (NULL, '".$client_id."', NULL, '', '', '".$client_auth_code."', '".$client_pkce_code."');";
        if (mysqli_query($this->connection, $query)) {
            return Array(
                'success' => true,
                'client_auth_code' => $client_auth_code
            );
        } else {
            return Array(
                'success' => false,
                'client_auth_code' => NULL
            );
        }
    }

    function updateClient($client_auth_code, $client_pkce_code) {
        $query = "SELECT `client_pkce_code`,`client_id` FROM `meesho`.`client` WHERE `client_auth_code` = '".$client_auth_code."';";
        $data = mysqli_query($this->connection, $query);
        if (mysqli_num_rows($data) == 0) {
            return false;
        } else {
            $client_data = mysqli_fetch_row($data);
            $methodUtils = new methodUtility();
            if($methodUtils->evaluateKey($client_data[0],$client_pkce_code)) {
//                $client_access_token = (hash('haval256,5', uniqid())).(hash('sha512', uniqid()));
//                $client_refresh_token = (hash('haval192,4', uniqid())).(hash('snefru256', uniqid()));
//                $query = "UPDATE `meesho`.`client` SET `client_access_token` = '".$client_access_token."', `client_refresh_token` = '".$client_refresh_token."' WHERE `client_pkce_code` = '".$client_pkce_code."' AND `client_auth_code` = '".$client_auth_code."';";
//                if (mysqli_query($this->connection, $query)) {
//                    return Array(
//                        'success' => false,
//                        'client_auth_code' => NULL
//                    );
//                } else {
//                    return Array(
//                        'success' => false,
//                        'client_auth_code' => NULL
//                    );
//                }
                return Array(
                    'success' => true,
                    'client_id' => $client_data[1]
                );
            }else{
                return Array(
                    'success' => false,
                    'client_id' => NULL
                );
            }
        }
    }

    function insertUser($account_kit_auth_code, $client_id, $user_pkce_code){
        //STEP - 1 : GET THE ACCESS TOKEN
        $application_id = "1385258898259704";
        $application_secret = "a736286549761afbb22bdc4323861416";
        $access_token_url = "https://graph.accountkit.com/v1.0/access_token?grant_type=authorization_code&code=".$account_kit_auth_code."&access_token=AA|$application_id|$application_secret";
        $data = $this->parseURL($access_token_url);
        if (isset($data['error'])){
            return Array(
                'success' => false,
                'client_id' => NULL
            );
        } else {
            //STEP - 2 : GET VALUES OF THE USER AND CHECK WHTHER ITS FROM SAME APPLICATION ID
            $user_access_token = $data['access_token'];
            $graph_api_url = 'https://graph.accountkit.com/v1.0/me?access_token='.$user_access_token;
            $data = $this->parseURL($graph_api_url);
            if ($data['application']['id'] === '1385258898259704'){
                $account_kit_id = $data['id'];
                $account_kit_phone_number = $data['phone']['number'];
                //STEP - 3 : THEN SET VALUES TO THE DATABASE
                $query = "SELECT * FROM `meesho`.`user` WHERE `account_kit_phone` = '$account_kit_phone_number' LIMIT 1;";
                $data = mysqli_query($this->connection, $query);
                if (mysqli_num_rows($data) == 0) {
                    $user_auth_code = hash('sha256', uniqid());
                    $query = "INSERT INTO `meesho`.`user` (`user_id`, `account_kit_id`, `account_kit_phone`, `user_auth_code`, `account_kit_access_token`, `user_refersh_token`, `user_pkce_code`) VALUES (NULL, '$account_kit_id', '$account_kit_phone_number', '$user_auth_code', '', '', '$user_pkce_code');";
                    if (mysqli_query($this->connection, $query)) {
                        return Array(
                            'success' => true,
                            'user_auth_code' => $user_auth_code
                        );
                    } else {
                        return Array(
                            'success' => false,
                            'user_auth_code' => NULL
                        );
                    }
                } else {
                    $user_auth_code = hash('sha256', uniqid());
                    $query = "UPDATE `meesho`.`user` SET `user_auth_code` = '$user_auth_code', `user_pkce_code` = '$user_pkce_code' WHERE `account_kit_id` = '$account_kit_id';";
                    if (mysqli_query($this->connection, $query)) {
                        return Array(
                            'success' => true,
                            'user_auth_code' => $user_auth_code
                        );
                    } else {
                        return Array(
                            'success' => false,
                            'user_auth_code' => NULL
                        );
                    }
                }
            }
        }
///         $response = $request->send();
////        print_r($response);
//        echo 'DONE';
        //STEP - 2 : GET VALUES OF THE USER AND CHECK WHTHER ITS FROM SAME APPLICATION ID
        //STEP - 3 : THEN SET VALUES TO THE DATABASE
    }

}

?>