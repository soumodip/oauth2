<?php

namespace methods_encryption;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class methodUtility{

    function returnResponse($modified_key, Response $response, $method){
        $modified_key = $this->filterModifiedKey($modified_key, $method);
        $modified_key_array = Array(
            'key' => $modified_key
        );
        header("Content-Type: application/json");
        echo json_encode($modified_key_array);
    }

    function filterModifiedKey($modified_key, $method){
        // REPLACE ANY M0->M9
        $func_name_array = array("M0", "M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9");
        for ($i = 0; $i < count($func_name_array); $i++) {
            $modified_key = str_replace($func_name_array[$i], "", $modified_key);
        }
        $position = rand(0, strlen($modified_key));
        $modified_key = substr($modified_key,0,$position).$method.substr($modified_key, $position, (strlen($modified_key) - $position));
        $modified_key = substr(uniqid(), 0 , 5).$modified_key.substr(uniqid(), (strlen(uniqid()) - 5) , (strlen(uniqid())));
        return $modified_key;
    }

    function evaluateKey($database_encrypted_pkce_key, $client_pkce_key){
        $database_encrypted_pkce_key = substr($database_encrypted_pkce_key,5, strlen($database_encrypted_pkce_key)-10);
        $client_encrypted_pkce_key = '';
        if (strpos($database_encrypted_pkce_key,'M0') !== false) {
            $database_encrypted_pkce_key = str_replace('M0', '', $database_encrypted_pkce_key);
            $client_encrypted_pkce_key = $this->generateM0($client_pkce_key);
        }else if (strpos($database_encrypted_pkce_key,'M1') !== false) {
            $database_encrypted_pkce_key = str_replace('M1', '', $database_encrypted_pkce_key);
            $client_encrypted_pkce_key = $this->generateM1($client_pkce_key);
        }else if (strpos($database_encrypted_pkce_key,'M2') !== false) {
            $database_encrypted_pkce_key = str_replace('M2', '', $database_encrypted_pkce_key);
            $client_encrypted_pkce_key = $this->generateM2($client_pkce_key);
        }else if (strpos($database_encrypted_pkce_key,'M3') !== false) {
            $database_encrypted_pkce_key = str_replace('M3', '', $database_encrypted_pkce_key);
            $client_encrypted_pkce_key = $this->generateM3($client_pkce_key);
        }else if (strpos($database_encrypted_pkce_key,'M4') !== false) {
            $database_encrypted_pkce_key = str_replace('M4', '', $database_encrypted_pkce_key);
            $client_encrypted_pkce_key = $this->generateM4($client_pkce_key);
        }else if (strpos($database_encrypted_pkce_key,'M5') !== false) {
            $database_encrypted_pkce_key = str_replace('M5', '', $database_encrypted_pkce_key);
            $client_encrypted_pkce_key = $this->generateM5($client_pkce_key);
        }else if (strpos($database_encrypted_pkce_key,'M6') !== false) {
            $database_encrypted_pkce_key = str_replace('M6', '', $database_encrypted_pkce_key);
            $client_encrypted_pkce_key = $this->generateM6($client_pkce_key);
        }else if (strpos($database_encrypted_pkce_key,'M7') !== false) {
            $database_encrypted_pkce_key = str_replace('M7', '', $database_encrypted_pkce_key);
            $client_encrypted_pkce_key = $this->generateM7($client_pkce_key);
        }else if (strpos($database_encrypted_pkce_key,'M8') !== false) {
            $database_encrypted_pkce_key = str_replace('M8', '', $database_encrypted_pkce_key);
            $client_encrypted_pkce_key = $this->generateM8($client_pkce_key);
        }else if (strpos($database_encrypted_pkce_key,'M9') !== false) {
            $database_encrypted_pkce_key = str_replace('M9', '', $database_encrypted_pkce_key);
            $client_encrypted_pkce_key = $this->generateM9($client_pkce_key);
        }
        if ($database_encrypted_pkce_key === $client_encrypted_pkce_key) {
            return true;
        } else {
            return false;
        }
    }

    function generateM0($key) {
        $modified_key = hash('md5', $key);
        $modified_key = hash('sha256', $modified_key);
        $modified_key = hash('sha512', $modified_key);
        return $modified_key;
    }

    function generateM1($key) {
        $modified_key = hash('haval256,5', $key);
        $modified_key = hash('sha256', $modified_key);
        $modified_key = hash('snefru256', $modified_key);
        return $modified_key;
    }

    function generateM2($key) {
        $modified_key = hash('tiger128,3', $key);
        $modified_key = hash('ripemd256', $modified_key);
        $modified_key = hash('sha512', $modified_key);
        return $modified_key;
    }

    function generateM3($key) {
        $modified_key = hash('fnv1a32', $key);
        $modified_key = hash('joaat', $modified_key);
        $modified_key = hash('sha512', $modified_key);
        return $modified_key;
    }

    function generateM4($key) {
        $modified_key = hash('ripemd128', $key);
        $modified_key = hash('crc32b', $modified_key);
        $modified_key = hash('sha512', $modified_key);
        return $modified_key;
    }

    function generateM5($key) {
        $modified_key = hash('haval192,3', $key);
        $modified_key = hash('haval192,4', $modified_key);
        $modified_key = hash('sha512', $modified_key);
        return $modified_key;
    }

    function generateM6($key) {
        $modified_key = hash('sha384', $key);
        $modified_key = hash('whirlpool', $modified_key);
        $modified_key = hash('sha512', $modified_key);
        return $modified_key;
    }

    function generateM7($key) {
        $modified_key = hash('haval128,3', $key);
        $modified_key = hash('haval256,4', $modified_key);
        $modified_key = hash('sha512', $modified_key);
        return $modified_key;
    }

    function generateM8($key) {
        $modified_key = hash('sha384', $key);
        $modified_key = hash('haval224,5', $modified_key);
        $modified_key = hash('sha512', $modified_key);
        return $modified_key;
    }

    function generateM9($key) {
        $modified_key = hash('gost-crypto', $key);
        $modified_key = hash('sha384', $modified_key);
        $modified_key = hash('sha512', $modified_key);
        return $modified_key;
    }


    function M0(Request $request, Response $response){
        $key = $request->getParsedBody()['code'];
        $modified_key = $this->generateM0($key);
        $this->returnResponse($modified_key, $response,'M0');
    }

    function M1(Request $request, Response $response){
        $key = $request->getParsedBody()['code'];
        $modified_key = $this->generateM1($key);
        $this->returnResponse($modified_key, $response,'M1');
    }

    function M2(Request $request, Response $response){
        $key = $request->getParsedBody()['code'];
        $modified_key = $this->generateM2($key);
        $this->returnResponse($modified_key, $response,'M2');
    }

    function M3(Request $request, Response $response){
        $key = $request->getParsedBody()['code'];
        $modified_key = $this->generateM3($key);
        $this->returnResponse($modified_key, $response,'M3');
    }

    function M4(Request $request, Response $response){
        $key = $request->getParsedBody()['code'];
        $modified_key = $this->generateM4($key);
        $this->returnResponse($modified_key, $response,'M4');
    }

    function M5(Request $request, Response $response){
        $key = $request->getParsedBody()['code'];
        $modified_key = $this->generateM5($key);
        $this->returnResponse($modified_key, $response,'M5');
    }

    function M6(Request $request, Response $response){
        $key = $request->getParsedBody()['code'];
        $modified_key = $this->generateM6($key);
        $this->returnResponse($modified_key, $response,'M6');
    }

    function M7(Request $request, Response $response){
        $key = $request->getParsedBody()['code'];
        $modified_key = $this->generateM7($key);
        $this->returnResponse($modified_key, $response,'M7');
    }

    function M8(Request $request, Response $response){
        $key = $request->getParsedBody()['code'];
        $modified_key = $this->generateM8($key);
        $this->returnResponse($modified_key, $response,'M8');
    }

    function M9(Request $request, Response $response){
        $key = $request->getParsedBody()['code'];
        $modified_key = $this->generateM9($key);
        $this->returnResponse($modified_key, $response,'M9');
    }

}

?>