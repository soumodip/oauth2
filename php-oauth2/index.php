<?php

require 'controller/facebook.php';
require 'controller/methods.php';
require 'controller/database.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \facebook\FBUtility as FBUtility;
use \methods_encryption\methodUtility as methodUtility;
use \database_connection\databaseUtility as DBUtility;
use Slim\Views\PhpRenderer;

require 'vendor/autoload.php';

$app = new \Slim\App;
$container = $app->getContainer();
$container['renderer'] = new PhpRenderer("./public");

//THIS FILE CONTAINS THE LIST OF ALL ROUTES

//INDEX PAGE
$app->get('/', function (Request $request, Response $response) {
    echo file_get_contents('public/index.html');
});
//INDEX PAGE
$app->get('/facebook/code', function (Request $request, Response $response, $args) {
    return $this->renderer->render($response, "/facebook_code.php", $args);
});
//return $this->response->withStatus(200)->withHeader('Location', 'https://facebook.com');

//API CONTROLLERS
//$app->post('/api/facebook/code', function (Request $request, Response $response) {
//    $FBUtils = new FBUtility();
//    $FBUtils->validateFBCode($request, $response);
//});

$app->post('/hash/generate', function (Request $request, Response $response) {
    $methodUtils = new methodUtility();
    $methodUtils->M1($request, $response);
});

//CLIENT-ID GENERATION PROCESS
$app->post('/api/client_id/code', function (Request $request, Response $response) {
    $DBUtils = new DBUtility('localhost','root','macmini');
    $responseArray = $DBUtils->insertClient($request->getParsedBody()['client_pkce_code']);
    returnResponseJSON($responseArray, $response);
});

$app->post('/api/client_id/generate', function (Request $request, Response $response) {
    $DBUtils = new DBUtility('localhost','root','macmini');
    $responseArray = $DBUtils->updateClient($request->getParsedBody()['client_auth_code'], $request->getParsedBody()['client_pkce_code']);
    returnResponseJSON($responseArray, $response);
});

//USER SIGN-UP AND LOGIN PROCESS
$app->post('/api/facebook/code', function (Request $request, Response $response) {
    $DBUtils = new DBUtility('localhost','root','macmini');
    $DBUtils->insertUser($request->getParsedBody()['account_kit_auth_code'], $request->getParsedBody()['client_id'], $request->getParsedBody()['user_pkce_code']);
    //returnResponseJSON($responseArray, $response);
});

$app->run();

//FUNCTION TO RETURN RESPONSE
function returnResponseJSON($responseArray, Response $response){
    header("Content-Type: application/json");
    echo json_encode($responseArray);
}

?>