<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <style>
        body{
            font-family: 'Josefin Sans', sans-serif;
        }
        .middle-box {
            display: flex;
            align-items: center;
            justify-content: center;
        }
        ul li {
            padding: 10px 0px;
        }
        #accountKitFrom{
            visibility: hidden;
        }
        h3.center{
            text-align: center;
            max-width: 500px;
        }
        .center{
            text-align: center;
        }
    </style>
</head>
<body>
<div class="middle-box">
    <div class="middle">
        <h1 class="center">Account Kit Code</h1>
        <h3 class="center">
            <?php
                print_r($_GET["code"]);
            ?>
        </h3>
        <h1 class="center">Account Kit Status</h1>
        <h3 class="center">
            <?php
                print_r($_GET["status"]);
            ?>
        </h3>
    </div>
</div>
</body>
</html>