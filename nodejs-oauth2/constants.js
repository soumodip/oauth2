var data = {
  user_name: "soumodippaul@gmail.com",
  user_password: "password",
  user_key: "",
  user_code: "",
  user_access_token: "",
  user_info: {
    Address: "XXXX",
    School: "XXXX",
    City: "XXXX"
  },
  methods: [
    "M1",
    "M2",
    "M3"
  ],
  access_token_code: [
    "UwVYY4",
    "KDWWVF",
    "4nardF",
    "0hcYzn",
    "jDrkMO",
    "k5p5PT",
    "3XWT2T",
    "qpdbBp",
    "E1GhDL",
    "4lc4hT",
    "UZbSUL",
    "RyFZpw",
    "lVAoGv",
    "UuZ09W",
    "94miyW",
    "725oJK",
    "muVSfX",
    "qlanD0",
    "ltnzcX",
    "s1EUEs",
    "p60IHX",
    "vEZ3ka",
    "4hu6t5",
    "puymLM",
    "7ZZbpM"
  ]
};

//GET THE LIST OF ALL CONSTANTS
exports.getConstants = function(){
  return data;
}

//SET THE USER_KEY
exports.setKey = function(user_key, user_code, user_access_token){
  data.user_key = user_key;
  data.user_code = user_code;
  data.user_access_token = user_access_token;
}
