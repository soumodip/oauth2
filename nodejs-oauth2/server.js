var express = require("express");
var body_parser =  require("body-parser");

var app = express();
app.use(body_parser.urlencoded({
    extended: true
}));

//INCLUDING THE PKCE CONTROLLERS
var pkce_customized_authorize = require("./pkce_customized/authorize");
var pkce_customized_api = require("./pkce_customized/api");
var encrypt = require('./pkce_customized/encrypt');

//DEFINE THE ROUTES
app.post('/custom/api/user/authorize', pkce_customized_authorize.authorizeUser);
app.post('/custom/api/user/access_token', pkce_customized_authorize.accessToken);
app.post('/custom/api/user', pkce_customized_api.getUser);
app.post('/custom/api/encrytion/M1', encrypt.M1);
app.post('/custom/api/encrytion/M2', encrypt.M2);
app.post('/custom/api/encrytion/M3', encrypt.M3);
app.post('/custom/api/encrytion/access_token', encrypt.getAccessToken);

//RUN THE APP
app.listen(3000, function(){
  console.log('OAUTH SERVER STARTED AT [PORT:3000]');
});
