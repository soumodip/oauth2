var constants = require("./../constants");
var uuid = require('uuid/v5');
var encrypt = require('./encrypt');

//STEP - 1 : AUTHORIZE THE USER
exports.authorizeUser = function(req, res){
  if (req.body.name !== undefined && req.body.password !== undefined && req.body.key !== undefined ) {
    if (req.body.name === constants.getConstants().user_name && req.body.password === constants.getConstants().user_password) {
      const code = uuid(new Date().getTime().toString(), uuid.URL);
      constants.setKey(req.body.key, code, "");
      res.status(200).json({
        success: true,
        code: code
      });
    } else {
      res.status(200).json({
        success: false,
        message: "USERNAME_OR_PASSWORD_DO_NOT_MATCH"
      });
    }
  }else{
    res.status(200).json({
      success: false,
      message: "MISSING_PARAMETERS"
    });
  }
};

//STEP - 2 : EXCHANGE CODE FOR ACCESS_TOKEN
exports.accessToken = function(req, res){
  if (req.body.code !== undefined && req.body.encrypted_key !== undefined) {
    if (req.body.code === constants.getConstants().user_code) {
      var encrypted_data = encrypt.getEncryptedKeyAndMethod(req.body.encrypted_key);
      if (encrypted_data !== null) {
        var method = encrypted_data.method;
        var encrypted_key = encrypted_data.encrypted_key;
        var user_encrypted_key;
        switch (method) {
          case constants.getConstants().methods[0]:
            user_encrypted_key = encrypt.generateM1(constants.getConstants().user_key, false);
            break;
          case constants.getConstants().methods[1]:
            user_encrypted_key = encrypt.generateM2(constants.getConstants().user_key, false);
            break;
          case constants.getConstants().methods[2]:
            user_encrypted_key = encrypt.generateM3(constants.getConstants().user_key, false);
            break;
          default:
            user_encrypted_key = encrypt.generateM1(constants.getConstants().user_key, false);
        }
        if (encrypted_key === user_encrypted_key) {
          const access_token_data = encrypt.processAccessToken(8);
          res.status(200).json({
            success: true,
            access_token_code: access_token_data.access_token_code
          });
        } else {
          res.status(200).json({
            success: false,
            message: "ENCRYTED_KEY_DO_NOT_MATCH"
          });
        }
      } else {
        res.status(200).json({
          success: false,
          message: "INVALID_ENCRYTED_KEY"
        });
      }
    } else {
      res.status(200).json({
        success: false,
        message: "INVALID_CODE"
      });
    }
  } else {
    res.status(200).json({
      success: false,
      message: "MISSING_PARAMETERS"
    });
  }
};
