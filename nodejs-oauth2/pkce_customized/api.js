var constants = require("./../constants");
var encrypt = require('./encrypt');
var crypto = require('crypto');

exports.getUser =  function(req, res){
  if (req.body.access_token !== undefined && req.body.encrypted_key !== undefined) {
    if (req.body.access_token === crypto.createHash('sha256').update(constants.getConstants().user_access_token).digest('base64')) {
      var encrypted_data = encrypt.getEncryptedKeyAndMethod(req.body.encrypted_key);
      if (encrypted_data !== null) {
        var encrypted_key = encrypted_data.encrypted_key;
        var method = encrypted_data.method;
        var user_encrypted_key;
        switch (method) {
          case constants.getConstants().methods[0]:
            user_encrypted_key = encrypt.generateM1(constants.getConstants().user_key, false);
            break;
          case constants.getConstants().methods[1]:
            user_encrypted_key = encrypt.generateM2(constants.getConstants().user_key, false);
            break;
          case constants.getConstants().methods[2]:
            user_encrypted_key = encrypt.generateM3(constants.getConstants().user_key, false);
            break;
          default:
            user_encrypted_key = encrypt.generateM1(constants.getConstants().user_key, false);
        }
        if (encrypted_key === user_encrypted_key) {
          res.status(200).json({
            success: true,
            data: constants.getConstants().user_info
          });
        } else {
          res.status(200).json({
            success: false,
            message: "ENCRYTED_KEY_DO_NOT_MATCH"
          });
        }
      } else {
        res.status(200).json({
          success: false,
          message: "INVALID_ENCRYTED_KEY"
        });
      }
    } else {
      res.status(200).json({
        success: false,
        message: "INVALID_ACCESS_TOKEN"
      });
    }
  }else{
    res.status(200).json({
      success: false,
      message: "MISSING_PARAMETERS"
    });
  }
};
