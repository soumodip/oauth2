var crypto = require('crypto');
var uuid = require('uuid/v5');
var encrypt = require('./encrypt');
var constants = require("./../constants");

//GET VALUES OF VAROIUS CRYPTOGRAPHIC FUNCTION
exports.M1 = function(req, res){
  if (req.body.key !==  undefined) {
    res.status(200).json({
      success: true,
      encrypted_key: encrypt.generateM1(req.body.key, true)
    });
  } else {
    res.status(200).json({
      success: false,
      message: "MISSING_PARAMETERS"
    });
  }
}

exports.M2 = function(req, res){
  if (req.body.key !==  undefined) {
    res.status(200).json({
      success: true,
      encrypted_key: encrypt.generateM2(req.body.key, true)
    });
  } else {
    res.status(200).json({
      success: false,
      message: "MISSING_PARAMETERS"
    });
  }
}

exports.M3 = function(req, res){
  if (req.body.key !==  undefined) {
    res.status(200).json({
      success: true,
      encrypted_key: encrypt.generateM3(req.body.key, true)
    });
  } else {
    res.status(200).json({
      success: false,
      message: "MISSING_PARAMETERS"
    });
  }
}

exports.getEncryptedKeyAndMethod = function(key){
  var encrypted_key = key.substr(5, (key.length - 10));
  for (var i = 0; i < constants.getConstants().methods.length; i++) {
    if(encrypted_key.indexOf(constants.getConstants().methods[i]) !== -1){
      return {
        encrypted_key: encrypted_key.replace(constants.getConstants().methods[i], ''),
        method: constants.getConstants().methods[i]
      };
    }
  }
  return null;
}

function generateRandomString(len){
  var random_str = "";
  var possible_str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < len; i++){
    random_str += possible_str.charAt(Math.floor(Math.random() * possible_str.length));
  }
  return random_str;
}

function customizeEncrytedKey(encrypted_key, method_name){
  var customized_encrypted_key = generateRandomString(5) + encrypted_key + generateRandomString(5);
  customized_encrypted_key = encrypt.stripFunctionNames(customized_encrypted_key);
  var position = Math.floor(Math.random() * encrypted_key.length) + 1;
  customized_encrypted_key = customized_encrypted_key.substr(0,position) + method_name + customized_encrypted_key.substr(position, customized_encrypted_key.length);
  return customized_encrypted_key;
}

//SET OF VARIOUS CRYPTOGRAPHIC FUNCTION
exports.generateM1 = function(key, customise_flag){
  var encrypted_key = crypto.createHash('sha256').update(key).digest('base64');
  if (customise_flag) {
    return customizeEncrytedKey(encrypted_key, 'M1');
  } else {
    return encrypted_key;
  }
}

exports.generateM2 = function(key, customise_flag){
  var encrypted_key = crypto.createHash('md5').update(key).digest("hex");
  if (customise_flag) {
    return customizeEncrytedKey(encrypted_key, 'M2');
  } else {
    return encrypted_key;
  }
}

exports.generateM3 = function(key, customise_flag){
  var encrypted_key = uuid(key, uuid.URL);
  if (customise_flag) {
    return customizeEncrytedKey(encrypted_key, 'M3');
  } else {
    return encrypted_key;
  }
}

exports.stripFunctionNames = function(key){
  for (var i = 0; i < (constants.getConstants().methods).length; i++) {
    key = key.replace(constants.getConstants().methods[i], '');
  }
  return key;
}

exports.processAccessToken = function(len){
  var access_token = '';
  var access_token_code = '';
  var temp;
  for (var i = 0; i < len; i++){
    temp = parseInt(Math.floor(Math.random() * 10));
    access_token += constants.getConstants().access_token_code[temp];
    access_token_code += temp;
  }
  constants.setKey(constants.getConstants().user_key, "", access_token);
  return {
    access_token_code: access_token_code
  }
}

exports.getAccessToken = function(req, res){
  var sha_access_token = crypto.createHash('sha256').update(constants.getConstants().user_access_token).digest('base64');
  res.status(200).json({
    success: true,
    access_token: sha_access_token
  });
}
