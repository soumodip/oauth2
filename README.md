<h2><b>Oauth 2.0</b></h2>
<p><i style="font-size:18px;"><b>Oauth 2.0</b></i> shows the implementation of authenticating users via Facebook's Account Kit following the RFC guides. The project has been implementedin both NodeJS and PHP.
</p>
<hr/>
<h3><b>Tips</b></h3>
<p>I would advice you to go through the OAuth 2.0 protocol procedures ( i.e. read RFC documents of IETF )</p>
<hr/>
<p>Developed by <a href="http://www.soumodippaul.com/">Soumodip Paul</a></p>